﻿using System;
using System.Collections.Generic;

using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV.Structure;
using Emgu.CV;
using Recognition.SkinDetector;
using System.Collections;
using System.Threading;
using System.Collections.Concurrent;

namespace Recognition
{
    public partial class WindowApp : Form
    {

        IColorSkinDetector skinDetector;
        Image<Bgr, Byte> currentFrame, currentFrameCopy;
        Capture grabber;
        AdaptiveSkinDetector detector;
        int frameWidth, frameHeight;
        Hsv hsv_min, hsv_max;
        Ycc YCrCb_min, YCrCb_max;
        Seq<Point> hull, filteredHull;
        Seq<MCvConvexityDefect> defects;
        MCvConvexityDefect[] defectArray;
        ConcurrentQueue<List<XYHolder>> queue;
        MCvBox2D box;

        
        public WindowApp()
        {
            InitializeComponent();
            grabber = new Emgu.CV.Capture();          
            grabber.QueryFrame();
            frameWidth = grabber.Width;
            frameHeight = grabber.Height;            
            detector = new AdaptiveSkinDetector(1, AdaptiveSkinDetector.MorphingMethod.ERODE);
            hsv_min = new Hsv(0, 45, 0); 
            hsv_max = new Hsv(20, 255, 255);            
            YCrCb_min = new Ycc(0, 131, 80);
            YCrCb_max = new Ycc(255, 185, 135);
            box = new MCvBox2D();
            queue = new ConcurrentQueue<List<XYHolder>>();

            //Thread workerThread = new Thread(this.printQueue);
            //workerThread.Start();

            Application.Idle += new EventHandler(FrameGrabber);                        
        }

        void FrameGrabber(object sender, EventArgs e)
        {
            currentFrame = grabber.QueryFrame();
            if (currentFrame != null)
            {
                currentFrameCopy = currentFrame.Copy();
                skinDetector = new YCrCbSkinDetector();
                Image<Gray, Byte> skin = skinDetector.DetectSkin(currentFrameCopy,YCrCb_min,YCrCb_max);
                ExtractContourAndHull(skin);             
                Draw();
                imageBoxSkin.Image = skin;
                imageBoxFrameGrabber.Image = currentFrame;
            }
        }
               
        public void printQueue()
        {
            while (true)
            {
                if (queue.Count() != 0)
                {
                    List<XYHolder> l = new List<XYHolder>();
                    queue.TryDequeue(out l);
                    for (int i = 0; i < l.Count; i++)
                    {
                        Console.WriteLine(l.ElementAt(i).Xvalue + " ### " + l.ElementAt(i).Xvalue);
                    }
                    Console.WriteLine("### Next capture");

                }
            }
        }
        private void ExtractContourAndHull(Image<Gray, byte> skin)
        {
            using (MemStorage storage = new MemStorage())
            {
                Contour<Point> contours = skin.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_LIST, storage);
                Contour<Point> biggestContour = null;

                Double Result1 = 0;
                Double Result2 = 0;
                while (contours != null)
                {
                    Result1 = contours.Area;
                    if (Result1 > Result2)
                    {
                        Result2 = Result1;
                        biggestContour = contours;
                    }
                    contours = contours.HNext;
                }

                if (biggestContour != null)
                {
                    Contour<Point> currentContour = biggestContour.ApproxPoly(biggestContour.Perimeter * 0.0025, storage);
                    biggestContour = currentContour;
                    hull = biggestContour.GetConvexHull(Emgu.CV.CvEnum.ORIENTATION.CV_CLOCKWISE);
                    box = biggestContour.GetMinAreaRect();
                    PointF[] points = box.GetVertices();

                    Point[] ps = new Point[points.Length];
                    for (int i = 0; i < points.Length; i++)
                        ps[i] = new Point((int)points[i].X, (int)points[i].Y);

                    filteredHull = new Seq<Point>(storage);
                    for (int i = 0; i < hull.Total; i++)
                    {
                        if (Math.Sqrt(Math.Pow(hull[i].X - hull[i + 1].X, 2) + Math.Pow(hull[i].Y - hull[i + 1].Y, 2)) > box.size.Width / 10)
                        {
                            filteredHull.Push(hull[i]);
                        }
                    }

                    defects = biggestContour.GetConvexityDefacts(storage, Emgu.CV.CvEnum.ORIENTATION.CV_CLOCKWISE);
                    defectArray = defects.ToArray();
                }
            }
        }

        private void Draw()
        {
            #region defects drawing
            List<XYHolder> list = new List<XYHolder>();
            for (int i = 0; i < defects.Total; i++)
            {
                PointF startPoint = new PointF((float)defectArray[i].StartPoint.X, (float)defectArray[i].StartPoint.Y);
                PointF depthPoint = new PointF((float)defectArray[i].DepthPoint.X, (float)defectArray[i].DepthPoint.Y);

                LineSegment2D startDepthLine = new LineSegment2D(defectArray[i].StartPoint, defectArray[i].DepthPoint);
                LineSegment2D depthEndLine = new LineSegment2D(defectArray[i].DepthPoint, defectArray[i].EndPoint);

                CircleF startCircle = new CircleF(startPoint, 5f);
                CircleF depthCircle = new CircleF(depthPoint, 5f);

                if ((startCircle.Center.Y < box.center.Y || depthCircle.Center.Y < box.center.Y) && (startCircle.Center.Y < depthCircle.Center.Y) && (Math.Sqrt(Math.Pow(startCircle.Center.X - depthCircle.Center.X, 2) + Math.Pow(startCircle.Center.Y - depthCircle.Center.Y, 2)) > box.size.Height / 6.5))
                {
                    list.Add(new XYHolder { Xvalue = startPoint, Yvalue = depthPoint });
                    currentFrame.Draw(startDepthLine, new Bgr(Color.Green), 2);
                }
        
                currentFrame.Draw(startCircle, new Bgr(Color.Red), 2);
                currentFrame.Draw(depthCircle, new Bgr(Color.Yellow), 5);
            }
            //put all Finger Points held in list into Queue
            queue.Enqueue(list);


            //test print
            //if (queue.Count() != 0)
            //{
            //    List<XYHolder> l = q.Dequeue();
            //    for (int i=0; i < l.Count; i++)
            //    {
            //        Console.WriteLine(l.ElementAt(i).Xvalue + " ### "  + l.ElementAt(i).Xvalue);

            //    }
            //    Console.WriteLine("### Next capture");

            //}

            #endregion
        }
                                      
    }

    public class XYHolder
    {
        public PointF Xvalue { get; set; }
        public PointF Yvalue { get; set; }
    }
}
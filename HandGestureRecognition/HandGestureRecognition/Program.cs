﻿using System;
using System.Windows.Forms;

namespace Recognition
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new WindowApp());
        }
    }
}

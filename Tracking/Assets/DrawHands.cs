﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawHands : MonoBehaviour {
    public Shader shader;



 
    public List<Vector3> points;
    public float distFromPlayer = 2.0f;

    public float visionDistance;

    public int N = 14;

    public bool init = true;

    //void Start () {
    //points = new List<Vector3>( new Vector3[28]);
    //}


    void Update () {
        if (init)
        {
            for (int i = 0; i < N*2 ; i++)
            {
                createLine(points[i], points[i + 1]);
                i++;
            }
        }
       

    }


    private void createLine(Vector3 start, Vector3 end)
    {
        Vector3 pointA = transform.position + transform.forward * distFromPlayer ;
        Vector3 pointB = transform.position + transform.forward * distFromPlayer ;
        Vector3 rotatedA = transform.rotation* start+ pointA;
        Vector3 rotatedB = transform.rotation * end + pointB;
        Debug.DrawLine(rotatedA, rotatedB);
    }
}


﻿using UnityEngine;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using HandGestureRecognition;

public class Tracker : MonoBehaviour {

	public int udpListenPort = 11111;

	private UdpClient udpListener;
	private IPEndPoint ipEndPoint;
    private CameraControll CControl;
    private UnityHandGestReco handRecognition;

	public struct HeadPose {
		public double yaw;
		public double pitch;
		public double roll;
	}
	public HeadPose currentHeadPose;
	Queue<HeadPose> headPoseQueue = new Queue<HeadPose>();
	Queue<string> udpErrorsQueue = new Queue<string>(); 

	// Use this for initialization
	void Start() {
        CControl = Camera.main.GetComponent<CameraControll>();
        handRecognition = new UnityHandGestReco();
        try {
			initializeUdp();
		} catch (SocketException e) {
			Debug.LogError(e.Message);
		}
	}

	// Update is called once per frame
	void Update() {
        handRecognition.FrameGrabber();
		updateHeadPose();
        CControl.UpdateFunction(currentHeadPose.pitch,currentHeadPose.yaw,currentHeadPose.roll);

		Debug.Log(string.Format("Yaw: {0:0.00} Pitch: {1:0.00} Roll: {2:0.00}",
			currentHeadPose.yaw, currentHeadPose.pitch, currentHeadPose.roll));
	}

	public void OnApplicationQuit() {
		udpListener.Close();
	}

	private void updateHeadPose() {
		lock (headPoseQueue) {
			if (headPoseQueue.Count > 0) {
				currentHeadPose = headPoseQueue.Dequeue();
			}
		}
		lock (udpErrorsQueue) {
			if (udpErrorsQueue.Count > 0) {
				Debug.LogError(udpErrorsQueue.Dequeue());
			}
		}
	}

	private void initializeUdp() {
		udpListener = new UdpClient(udpListenPort);
		ipEndPoint = new IPEndPoint(IPAddress.Any, udpListenPort);
		Thread udpThread = new Thread(updateUdpListner);
		udpThread.Start();
	}

	private void updateUdpListner() {
		try {
			while (true) {
				byte[] bytes = udpListener.Receive(ref ipEndPoint);
				// wdg. protokolu opentrack, powinno byc 6 doubli, 3 ostatnie nas interesuja
				if (bytes.Length != 6 * sizeof(double)) {
					throw new ProtocolViolationException("Invalid head pose data, array of 6 doubles expected");
				}
				HeadPose data;
				data.yaw = System.BitConverter.ToDouble(bytes, 3 * sizeof(double));
				data.pitch = System.BitConverter.ToDouble(bytes, 4 * sizeof(double));
				data.roll = System.BitConverter.ToDouble(bytes, 5 * sizeof(double));
				lock (headPoseQueue) {
					headPoseQueue.Enqueue(data);
				}
			}
		} catch (System.Exception e) {
			lock (udpErrorsQueue) {
				udpErrorsQueue.Enqueue(e.Message);
			}
		}
	}
    
}

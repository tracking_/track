﻿using UnityEngine;
using System.Collections;

public class CameraControll : MonoBehaviour {

    public float roll = 10.0f;
    public float pitch = 10.0f;
    public float yaw = 10.0f;

    public float coeffroll = 1.0f;
    public float coeffpitch = 1.0f;
    public float coeffyaw = 1.0f;

    void UpdateFunction()
    {
        /*Testing*/
        float sinx = pitch * Mathf.Sin(Time.time * coeffpitch);
        float siny = yaw * Mathf.Sin(Time.time * coeffyaw);
        float sinz = roll * Mathf.Sin(Time.time * coeffroll);

        var addRot = new Vector3(-sinx, siny, -sinz);
        Camera.main.transform.rotation = Quaternion.Euler(Camera.main.transform.rotation.eulerAngles + addRot);     
    }
    public void UpdateFunction(double pitch, double yaw, double roll)
	{
		var headRot = Vector3.up * (float)yaw + Vector3.forward * (float)pitch + Vector3.right * -(float)roll;
		Camera.main.transform.rotation = Quaternion.Euler(Camera.main.transform.rotation.eulerAngles + headRot);
    }
}
